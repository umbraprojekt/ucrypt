**uCrypt** is a simple password encryptor/decryptor. It can be used to avoid storing passwords in configuration files in an unencrypted form. It uses RSA keys to encrypt/decrypt passwords.

```
const uCrypt = require("ucrypt");
const expect = require("chai").expect;

describe("uCrypt", () => {
    const publicKey = "/path/to/publicKey.pem";
    const privateKey = "/path/to/privateKey.pem";
    const password = "superSecretPassword";

    it("should encrypt and decrypt back asynchronously", () => {
        return uCrypt.encrypt(password, privateKey)
            .then(encrypted => uCrypt.decrypt(encrypted, privateKey))
            .then(decrypted => expect(decrypted).to.equal(password));
    });

    it("should encrypt and decrypt back synchronously", () => {
        const encrypted = uCrypt.encryptSync(password, publicKey);
        const decrypted = uCrypt.decryptSync(encrypted, privateKey);
        expect(decrypted).to.equal(password);
    });
});
```

## Install

```
npm install --save ucrypt
```

## How to use

### API

**uCrypt** requires SSH keys for password encryption and decryption. You can generate a public/private key pair using the following commands:

```
$ openssl genrsa -out privateKey.pem 2048
$ openssl rsa -in privateKey.pem -pubout > publicKey.pem
```

Passwords can be encrypted and decrypted back using the generated key pair in the following fashion:

```
const uCrypt = require("ucrypt");

const publicKey = "/path/to/publicKey.pem";
const privateKey = "/path/to/privateKey.pem";
const password = "superSecretPassword";

// async encrypt
uCrypt.encrypt(password, publicKey)
    .then(encrypted => {
        // do something with the result
    });

// sync encrypt
const encrypted = uCrypt.encryptSync(password, publicKey);

// async decrypt
uCrypt.decrypt(encrypted, privateKey)
    .then(decrypted => {
        // do something with the result
    });

// sync decrypt
const derypted = uCrypt.decryptSync(encrypted, privateKey);
```

**uCrypt** includes TypeScript typings, so it can also be used in TypeScript applications:

```
import {encrypt, encryptSync, decrypt, decryptSync} from "ucrypt";

const publicKey = "/path/to/publicKey.pem";
const privateKey = "/path/to/privateKey.pem";
const password = "superSecretPassword";

// async encrypt
encrypt(password, publicKey)
    .then(encrypted => {
        // do something with the result
    });

// sync encrypt
const encrypted = encryptSync(password, publicKey);

// async decrypt
decrypt(encrypted, privateKey)
    .then(decrypted => {
        // do something with the result
    });

// sync decrypt
const derypted = decryptSync(encrypted, privateKey);
```

### Command line

**uCrypt** provides a command line tool. In order to use it, install **uCrypt** globally:

```
npm install --global ucrypt
```

The package exposes two commands: `encrypt` and `decrypt`. Both accept two arguments: path to the key file and the password to process. They print the result to the console.

```
$ encrypt /path/to/publicKey.pem openTextPassword
$ decrypt /path/to/privateKey.pem encryptedPassword
```
