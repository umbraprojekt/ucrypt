const crypto = require("crypto");
const fs = require("fs");

const getKey = (pathToKey) => {
	return new Promise((resolve, reject) => {
		fs.access(pathToKey, fs.constants.R_OK, (error) => {
			if (error) {
				reject(new Error(`File ${pathToKey} does not exist or is not accessible.`));
			}
			fs.readFile(pathToKey, "utf8", (error, data) => {
				if (error) {
					reject(error);
				} else {
					resolve(data);
				}
			});
		});
	});
};

const getKeySync = (pathToKey) => {
	try {
		fs.accessSync(pathToKey, fs.constants.R_OK);
	} catch (e) {
		throw new Error(`File ${pathToKey} does not exist or is not accessible.`);
	}

	return fs.readFileSync(pathToKey, "utf8");
};

const publicEncrypt = (toEncrypt, publicKey) => {
	const buffer = Buffer.from(toEncrypt);
	const encrypted = crypto.publicEncrypt({
		key: publicKey,
		oaepHash: "sha256",
		padding: crypto.constants.RSA_PKCS1_OAEP_PADDING
	}, buffer);
	return encrypted.toString("base64");
};

const privateDecrypt = (toDecrypt, privateKey) => {
	const buffer = Buffer.from(toDecrypt, "base64");
	const decrypted = crypto.privateDecrypt({
		key: privateKey,
		oaepHash: "sha256",
		padding: crypto.constants.RSA_PKCS1_OAEP_PADDING
	}, buffer);
	return decrypted.toString("utf8");
};

const encrypt = (toEncrypt, pathToPublicKey) => {
	return getKey(pathToPublicKey)
		.then(publicKey => publicEncrypt(toEncrypt, publicKey));
};

const encryptSync = (toEncrypt, pathToPublicKey) => {
	const publicKey = getKeySync(pathToPublicKey);
	return publicEncrypt(toEncrypt, publicKey);
};

const decrypt = (toDecrypt, pathToPrivateKey) => {
	return getKey(pathToPrivateKey)
		.then(privateKey => privateDecrypt(toDecrypt, privateKey));
};

const decryptSync = (toDecrypt, pathToPrivateKey) => {
	const privateKey = getKeySync(pathToPrivateKey);
	return privateDecrypt(toDecrypt, privateKey);
};

module.exports = {
	encrypt,
	encryptSync,
	decrypt,
	decryptSync
};
