export declare const encrypt: (toEncrypt: string, pathToPublicKey: string) => Promise<string>;
export declare const encryptSync: (toEncrypt: string, pathToPublicKey: string) => string;
export declare const decrypt: (toDecrypt: string, pathToPrivateKey: string) => Promise<string>;
export declare const decryptSync: (toDecrypt: string, pathToPrivateKey: string) => string;
