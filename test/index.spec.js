const uCrypt = require("../src/index");
const expect = require("chai").expect;
const path = require("path");

describe("uCrypt", () => {
	const publicKey = path.join(__dirname, "resources/publicKey.pem");
	const privateKey = path.join(__dirname, "resources/privateKey.pem");
	const password = "superSecretPassword";

	it("should encrypt and decrypt back asynchronously", () => {
		return uCrypt.encrypt(password, privateKey)
			.then(encrypted => uCrypt.decrypt(encrypted, privateKey))
			.then(decrypted => expect(decrypted).to.equal(password));
	});

	it("should encrypt and decrypt back synchronously", () => {
		const encrypted = uCrypt.encryptSync(password, publicKey);
		const decrypted = uCrypt.decryptSync(encrypted, privateKey);
		expect(decrypted).to.equal(password);
	});
});
